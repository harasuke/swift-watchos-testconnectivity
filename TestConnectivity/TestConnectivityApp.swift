//
//  TestConnectivityApp.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 29/06/22.
//

import SwiftUI

@main
struct TestConnectivityApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

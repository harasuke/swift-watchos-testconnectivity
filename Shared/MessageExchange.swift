//
//  MessageExchange.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 29/06/22.
//

import UIKit

public class MessageExchange: NSObject, ObservableObject, NSSecureCoding {
    
    public static var supportsSecureCoding: Bool = true
    
    let id = UUID()
    var messageText: String = "Waiting..."
    
    func initWithData(messageText: String) {
        self.messageText = messageText
    }
    
    public required convenience init?(coder: NSCoder) {
        //decode the data
        guard let messageText = coder.decodeObject(forKey: "messageText") as? String
            else { return nil }
        self.init()
        self.initWithData(messageText: messageText as String)
    }
    
    public func encode(with coder: NSCoder) {
        //convert data to a set of bytes in form of key-value pairs
        coder.encode(self.messageText, forKey: "messageText")
    }

}

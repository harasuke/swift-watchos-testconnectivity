//
//  ContentView.swift
//  TestConnectivity WatchKit Extension
//
//  Created by Mirko Spinato on 29/06/22.
//

import SwiftUI
import CoreData

struct MainView: View {

    @StateObject var connect = ConnectionProvider()
    
    @FetchRequest(sortDescriptors: []) private var oldMsg: FetchedResults<Messages>
//    @FetchRequest(fetchRequest: getOldMessages()) var oldMsg: FetchedResults<Messages>
//    static func getOldMessages() -> NSFetchRequest<Messages> {
//        let request:NSFetchRequest<Messages> = Messages.fetchRequest()
////        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
////        request.sortDescriptors = [sortDescriptor]
//        return request
//    }
    
    var body: some View {
        NavigationView {
            VStack {
                Button(action: {
                    connect.sendJSONRequest("https://jsonplaceholder.typicode.com/todos/1")
                }, label: {Text("Send message")} )
                Text(connect.jsonData.title)
            }
        }.edgesIgnoringSafeArea(.bottom)
        
    }
}

struct MainView_Previews: PreviewProvider {
    private static var txt:ConnectionProvider = ConnectionProvider()
    static var previews: some View {
        MainView()
    }
}

//
//  MyViewer.swift
//  TetsConnectivity WatchKit Extension
//
//  Created by Mirko Spinato on 01/07/22.
//

import SwiftUI

struct MyViewer: View {
    
    @StateObject var connect:ConnectionProvider
    
    var body: some View {
        List(connect.results, id: \.id) { res in
            Text(res.title)
        }
    }
}

//
//  JSON_base.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 30/06/22.
//

import Foundation

class Response: NSObject, Codable, NSSecureCoding {
    static var supportsSecureCoding:Bool = true
    var listares: [Result] = [Result]()

    public required convenience init?(coder: NSCoder) {
        //decode the data
        guard let listares = coder.decodeObject(forKey: "listares") as? [Result]
            else { return nil }
        self.init()
        self.listares = listares
    }

    public func encode(with coder: NSCoder) {
        //convert data to a set of bytes in form of key-value pairs
        coder.encode(self.listares, forKey: "listares")
    }
}

class Result: NSObject, Codable, NSSecureCoding {
    static var supportsSecureCoding: Bool = true

    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var completed: Bool = true

    public required convenience init?(coder: NSCoder) {
        //decode the data
        guard let userId = coder.decodeInteger(forKey: "userId") as? Int,
              let id = coder.decodeInteger(forKey: "id") as? Int,
              let title = coder.decodeObject(forKey: "title") as? String,
              let completed = coder.decodeBool(forKey: "completed") as? Bool
            else { return nil }
        self.init()
        self.userId = userId
        self.id = id
        self.title = title
        self.completed = completed
    }

    public func encode(with coder: NSCoder) {
        //convert data to a set of bytes in form of key-value pairs
        coder.encode(self.userId, forKey: "userId")
        coder.encode(self.id, forKey: "id")
        coder.encode(self.title, forKey: "title")
        coder.encode(self.completed, forKey: "completed")
    }
}

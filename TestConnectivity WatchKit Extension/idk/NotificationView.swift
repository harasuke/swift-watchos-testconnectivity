//
//  NotificationView.swift
//  TestConnectivity WatchKit Extension
//
//  Created by Mirko Spinato on 29/06/22.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}

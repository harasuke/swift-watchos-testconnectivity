//
//  ContentView.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 29/06/22.
//

import SwiftUI

struct ContentView: View {

    @StateObject var connect = ConnectionProvider()

    var body: some View {
        VStack {
            Text(connect.incomingMessage)
            List(connect.oldMessages, id:\.self) { msg in
                Text(msg)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

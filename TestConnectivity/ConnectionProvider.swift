//
//  ConnectivityProvider.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 29/06/22.
//

import UIKit
import WatchConnectivity
import CoreData

class ConnectionProvider: NSObject, WCSessionDelegate, ObservableObject {
    
    private let session: WCSession = WCSession.default
    @Published var isReachable = false
    
    @Published var incomingMessage:String = ""
    @Published var oldMessages:[String] = []
    
    init(session:WCSession = .default) {
            super.init()
            self.session.delegate = self
        if WCSession.isSupported() {
            self.session.activate()
            print("ConnectionProvider started on iPhone")
        }
    }
    
    
    func sessionDidBecomeInactive(_ session: WCSession) {}
    func sessionDidDeactivate(_ session: WCSession) {}
    
    func send(msg: [String: Any]) -> Void {
        session.sendMessage(msg, replyHandler: nil) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        DispatchQueue.main.async {
            self.isReachable = session.isReachable
            print("iOS should be reachable")
        }
    }

    
    //MARK: Arrive of a message from the Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            
            NSKeyedUnarchiver.setClass(NSString.self, forClassName: "JSONRequest")
            let unarchivedMessage = try! NSKeyedUnarchiver.unarchivedObject(ofClass: NSString.self, from: message["url"] as! Data)
                
            self.incomingMessage = unarchivedMessage! as String
            self.oldMessages.append(self.incomingMessage)
            Task { await self.sendJSON(self.incomingMessage) }
            
        }
        
    }
    
    
    //MARK: Send JSON to the Watch
    func sendJSON(_ _url: String) async {
        if session.isReachable {
                    
            guard let url = URL(string: _url)
            else { print("Invalid URL")
                return
            }
            do {
                let (data, _) = try await URLSession.shared.data(from: url)
                if let jsonString = try? JSONDecoder().decode(Result.self, from: data) {

                    NSKeyedArchiver.setClassName("JSONResponse", for: Result.self)
                    let encoded = try! NSKeyedArchiver.archivedData(withRootObject: jsonString, requiringSecureCoding: true)
                    self.session.sendMessage(["jsonData": encoded], replyHandler: nil)

                }
            } catch { print("Invalid data")
                return
            }
            
        }
    }
    
}

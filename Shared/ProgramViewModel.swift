//
//  ProgramViewModel.swift
//  TestConnectivity
//
//  Created by Mirko Spinato on 29/06/22.
//

import UIKit

final class ProgramViewModel: ObservableObject {
    private(set) var connectivityProvider: ConnectionProvider
    
    init(connectivityProvider: ConnectionProvider) {
        self.connectivityProvider = connectivityProvider
    }
}

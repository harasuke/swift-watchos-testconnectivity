## WatchConnectivity example

This is an example for using the WatchConnectivity framework and send data back and forth iPhone & Apple Watch.
In this test, the Apple Watch sends a url string to the iPhone. The iPhone will take care to resolve it, encode the JSON response, and send back the Object to the Apple Watch


The class "MesageExchange.swift" was used as an example for sending a custom object to the watch.
It works but the program need to be adjusted


*Works on WatchOS 8.5*

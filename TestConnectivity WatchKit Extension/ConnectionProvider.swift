//
//  ConnectionProvider.swift
//  TestConnectivity WatchKit Extension
//
//  Created by Mirko Spinato on 01/07/22.
//

import UIKit
import WatchConnectivity
import CoreData

class ConnectionProvider: NSObject, WCSessionDelegate, ObservableObject {
    
    private let session: WCSession = WCSession.default
    @Published var isReachable = false
    
    @Published var jsonData:Result = Result()
    
    init(session:WCSession = .default) {
            super.init()
            self.session.delegate = self
        if WCSession.isSupported() {
            self.session.activate()
            print("ConnectionProvider started on Watch")
        }
    }
    
    func send(msg: [String: Any]) -> Void {
        session.sendMessage(msg, replyHandler: nil) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        DispatchQueue.main.async {
            self.isReachable = session.isReachable
            print("WatchOS should be reachable")
        }
    }
    
    //MARK: Message received from the iPhone
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            
            NSKeyedUnarchiver.setClass(Result.self, forClassName: "JSONResponse")
            let unarchivedMessage = try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [Result.self, NSString.self, NSNumber.self], from: message["jsonData"] as! Data)
                
            self.jsonData = unarchivedMessage! as! Result
            
        }
    }
    
    func sendJSONRequest(_ messageText: String) {
        if session.isReachable {
                
                NSKeyedArchiver.setClassName("JSONRequest", for: NSString.self)
                let encoded = try! NSKeyedArchiver.archivedData(withRootObject: messageText, requiringSecureCoding: true)
                self.session.sendMessage(["url": encoded], replyHandler: nil)

        }
    }

}

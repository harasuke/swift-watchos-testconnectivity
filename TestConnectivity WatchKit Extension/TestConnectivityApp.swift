//
//  TestConnectivityApp.swift
//  TestConnectivity WatchKit Extension
//
//  Created by Mirko Spinato on 29/06/22.
//

import SwiftUI

@main
struct TestConnectivityApp: App {
    
    let persistenceController = PersistenceController.shared
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                MainView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
